import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  changerDePage(page: string) {
    if (page === 'plantes') {
      this.router.navigate(['/plantes']);
    } else if (page === 'taches') {
      this.router.navigate(['/taches']);
    } else if (page === 'agenda') {
      this.router.navigate(['/agenda']);
    } else if (page === 'capteurs') {
      this.router.navigate(['/capteurs']);
    } else if (page === 'animaux') {
      this.router.navigate(['/animaux']);
    } else if (page === 'map') {
      this.router.navigate(['/map']);
    }

  }
}
