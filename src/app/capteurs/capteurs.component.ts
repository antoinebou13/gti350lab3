import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-capteurs',
  templateUrl: './capteurs.component.html',
  styleUrls: ['./capteurs.component.css']
})
export class CapteursComponent implements OnInit {
  listeCapteurs: any[];
  listePlantes: any[];
  constructor() {
    this.listePlantes = [
      'Framboises',
      'Fraises',
      'Concombres',
      'Tomates',
      'Terre section 6',
      'Terre section 11'
    ];
    this.listeCapteurs = [
      {
        nom: 'Eau',
        id: 'W501',
        actif: true,
        img : 'opacity',
        msg: 'Tout va bien',
        couleur: 'blue'
      },
      {
        nom: 'PH',
        id: 'PH02',
        actif: false,
        img : 'exposure_neg_2',
        msg: 'Non configuré',
        couleur: 'darkviolet'
      },
      {
        nom: 'Soleil',
        id: 'S03',
        actif: true,
        img : 'brightness_5',
        msg: 'Manque de soleil',
        couleur: 'yellow'
      },
      {
        nom: 'Parasite',
        id: 'P42',
        actif: false,
        img : 'emoji_nature',
        msg: 'Connection non trouvée',
        couleur: 'darkgreen'
      },
    ];
  }

  ngOnInit() {
  }

}
