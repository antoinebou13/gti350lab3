import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-animaux',
  templateUrl: './animaux.component.html',
  styleUrls: ['./animaux.component.css']
})
export class AnimauxComponent implements OnInit {
  listeAnimaux: any[];
  constructor() {
    this.listeAnimaux = [
      {
        id: 'Vache01',
        nom: 'Suzie',
        age: '10',
        sexe: 'Femelle',
        medical: 'Rhume',
        photo: './assets/static/vache01.jpg',
      },
      {
        id: 'Mouton02',
        nom: 'George',
        age: '5',
        sexe: 'Male',
        medical: 'Aucune',
        photo: './assets/static/mouton02.jpg',
      },
      {
        id: 'Cochon03',
        nom: 'Charles',
        age: '4',
        sexe: 'Male',
        medical: 'Grippe',
        photo: './assets/static/cochon03.jpg',
      }
    ];
  }

  ngOnInit() {
  }

}
