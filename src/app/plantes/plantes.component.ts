import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-plantes',
  templateUrl: './plantes.component.html',
  styleUrls: ['./plantes.component.css']
})
export class PlantesComponent implements OnInit {
  listePlantes: any[];
  displayedColumns: string[] = ['type', 'humidite', 'heure', 'forme'];

  constructor() {
    this.listePlantes = [
      {
        type: 'Framboises',
        humidite: '78%',
        heure: '12:00',
        forme: 'mood',
      },
      {
        type: 'Fraises',
        humidite: '30%',
        heure: '13:17',
        forme: 'mood_bad',
      },
      {
        type: 'Concombres',
        humidite: '55%',
        heure: '15:40',
        forme: 'mood_bad',
      },
      {
        type: 'Tomates',
        humidite: '42%',
        heure: '16:29',
        forme: 'mood',
      }
    ];
  }

  ngOnInit() {
  }

}
