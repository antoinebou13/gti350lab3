import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-taches',
  templateUrl: './taches.component.html',
  styleUrls: ['./taches.component.css']
})
export class TachesComponent implements OnInit {
  listeTaches: any[];
  constructor(private router: Router) {
    this.listeTaches = [
      {
        nom: 'Arroser le blé',
        heure: '06:10',
      },
      {
        nom: 'Traire les vaches',
        heure: '07:00'
      },
      {
        nom: 'Nourrire les animaux',
        heure: '08:30'
      },
      {
        nom: 'Nettoyer enclot des vaches',
        heure: '12:00'
      },
      {
        nom: 'Livraison du blé',
        heure: '13h15'
      }
    ];
  }

  ngOnInit() {
  }


}
